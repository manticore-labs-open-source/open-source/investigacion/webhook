# **Webhook para dialogflow v1**
Aquí se muestra cómo crear un webhook para dialogflow v1.


## ¿Qué es un Webhook?

Un Webhook, es un método que permite alterar en tiempo real el funcionamiento de una aplicación web, dicha personalización normalmente se logra manejando la respuesta (response) mediante la implementación de métodos HTTP normalmente usando un POST los cuales son enviados desde nuestro webhook hacia una URL específica.

## Creando un webhook para dialogflow v1

### Requisitos 

* Un agente de dialogflow.
* Project ID del agente.
* Seleccionar V1 API de dialogflow.

    ![](imagenes/project.png)

* Developer access token.
* Un proyecto nest.js 
* El webhook deberá utilizar una dirección pública.

Todos estos requisitos se utilizarán en el transcurso del tutorial. 

## Información

Lo importante es realizar un POST para esto necesitamos un decorador.


``` TypeScript
@Controller()
```

Además, necesitamos realizar ciertas solicitudes HTTP, para esto importaremos HttpService o también podemos utilizar axios para esta práctica utilizaremos HttpService. 


``` TypeScript
import { HttpService} from "@nestjs/common";
```

Dialogflow utiliza la misma **baseURL** para todas las solicitudes que se realizaran al agente en este caso se utilizara **20150910** el cual es un protocolo base. Para más información puedes consultar la documentación de dialogflow. [Docs dialogflow](https://dialogflow.com/docs/reference/agent)

``` TypeScript
private baseURL: string = "https://api.dialogflow.com/v1/query?v=20150910";
```



## **Tutorial**

Se debe añadir el controller al módulo principal o al módulo en el que se va a usar, es importante importar HttpModule para poder utilizar los métodos. 

``` TypeScript
import { Module, HttpService, HttpModule } from "@nestjs/common";
import { WebhookController } from "./webhook.controller";


@Module({
    controllers: [WebhookController],
    providers: [],
    imports: [HttpModule]
})

export class WebhookModule{}
````

En cuanto al controller necesitamos implementar un método Post en el cual debemos setear el lenguaje de nuestro agente. Utilizando HttpService haremos post a nuestra baseURL con nuestra data y finalmente en la cabecera enviaremos los datos para la autorización, aquí enviaremos el token de nuestro agente. 




``` TypeScript
import { Controller, HttpService, Post, Body, HttpStatus, Res } from "@nestjs/common";


@Controller('webhook')

export class WebhookController {
     constructor(private readonly _httpService: HttpService){} 

    private baseURL: string = "https://api.dialogflow.com/v1/query?v=20150910";
    
    private token: string = "Ingresa tu token";
    
    @Post()
    enviarMensaje(
        @Res() res,
        @Body() info
        ) {
    
        const data = {
            query: info.message,
            lang: 'es',
            sessionId: info.date
        }

       this._httpService.post(this.baseURL,data,{headers:{Authorization: `Bearer ${this.token}`}})
        .subscribe((respuesta)=>{
            res.status(HttpStatus.OK).send(respuesta.data);
        });
    }

}
````


Finalmente seremos nosotros quienes realizamos la comunicación inicial con el agente. 








    

    








<a href="https://twitter.com/alimonse29" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @alimonse29 </a><br>


